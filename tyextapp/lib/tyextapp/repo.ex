defmodule Tyextapp.Repo do
  use Ecto.Repo,
    otp_app: :tyextapp,
    adapter: Ecto.Adapters.Postgres
end
