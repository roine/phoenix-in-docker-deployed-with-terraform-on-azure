defmodule TyextappWeb.PageController do
  use TyextappWeb, :controller

  def index(conn, _params) do
    posts = Tyextapp.Repo.all Tyextapp.Blog.Post
    render(conn, "index.html", posts: posts)
  end
end
