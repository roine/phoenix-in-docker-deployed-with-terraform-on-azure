variable "azure_region" {
  description = "Server Region"
  default     = "Australia South East"
}

variable "prod_group" {
  description = "Production group name"
  default     = "telerama"
}

variable "psql_server_admin_pass" {}
