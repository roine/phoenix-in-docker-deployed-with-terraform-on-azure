provider "azurerm" {}

# Create the resource group
resource "azurerm_resource_group" "telerama" {
  name     = "${var.prod_group}"
  location = "${var.azure_region}"
}

# Create the container registry
resource "azurerm_container_registry" "telerama" {
  name                = "teleramacr"
  resource_group_name = "${azurerm_resource_group.telerama.name}"
  location            = "${azurerm_resource_group.telerama.location}"
  sku                 = "Standard"
  admin_enabled       = true
}

# Create the db
resource "azurerm_postgresql_server" "telerama" {
  name                = "postgresql-server-1"
  location            = "${azurerm_resource_group.telerama.location}"
  resource_group_name = "${azurerm_resource_group.telerama.name}"

  sku {
    name     = "B_Gen5_1"
    capacity = 1
    tier     = "Basic"
    family   = "Gen5"
  }

  storage_profile {
    storage_mb            = 5120
    backup_retention_days = 7
    geo_redundant_backup  = "Disabled"
  }

  administrator_login          = "psqladminun"
  administrator_login_password = "${var.psql_server_admin_pass}"
  version                      = "9.6"
  ssl_enforcement              = "Enabled"
}

resource "azurerm_postgresql_database" "telerama" {
  name                = "telerama"
  resource_group_name = "${azurerm_resource_group.telerama.name}"
  server_name         = "${azurerm_postgresql_server.telerama.name}"
  charset             = "UTF8"
  collation           = "SQL_Latin1_General_CP1_CS_AS"
}
