PROJECT_NAME = phxapp
IMAGE_NAME = ${PROJECT_NAME}
AZURE_REGISTRY_NAME ?= tyextRegistry
DOCKER_IMAGE_NAME ?= ${AZURE_REGISTRY_NAME}.azurecr.io/${IMAGE_NAME}

.PHONY: help
help: ## This help dialog.
	@IFS=$$'\n' ; \
    help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##/:/'`); \
    printf "%-30s %s\n" "target" "help" ; \
    printf "%-30s %s\n" "------" "----" ; \
    for help_line in $${help_lines[@]}; do \
        IFS=$$':' ; \
        help_split=($$help_line) ; \
        help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
        help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
        printf '\033[36m'; \
        printf "%-30s %s" $$help_command ; \
        printf '\033[0m'; \
        printf "%s\n" $$help_info; \
	done

.PHONY: push
push: ## Push the docker image to the registry.
	@echo "--- :satellite: Pushing to docker registry :satellite:"
	@az login -u "${AZURE_USERNAME}" -p "${AZURE_PASSWORD}" --tenant ${AZURE_TENANTID}
	az acr login -n ${AZURE_REGISTRY_NAME}
	# Sends the folder's contents to Azure Container Registry, which uses the instructions in the Docker file to build the image, then tag it and store it.
	az acr build --registry ${AZURE_REGISTRY_NAME} --image ${IMAGE_NAME}:latest .

