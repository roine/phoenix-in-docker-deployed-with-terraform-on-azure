# ./Dockerfile

# Extend from the official Elixir image
FROM bitwalker/alpine-elixir-phoenix:1.8.1

# Create app directory and copy the Elixir projects into it
RUN mkdir /app
COPY tyextapp /app
WORKDIR /app

# Install postgres tool to check when postgres ready
RUN apk add postgresql-client

# Install iNotify (event passing) and Elixir miscs
RUN apk add inotify-tools \
    && mix local.rebar --force \
    && mix do compile

EXPOSE 80

CMD ["/app/entrypoint.sh"]