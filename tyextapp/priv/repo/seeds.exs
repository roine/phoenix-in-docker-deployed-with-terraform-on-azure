# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Tyextapp.Repo.insert!(%Tyextapp.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.


Tyextapp.Repo.insert(%Tyextapp.Blog.Post{title: "Jackson is king", views: 10000000})